const body = document.getElementsByTagName('body')[0];
const html = document.getElementsByTagName('html')[0];

// viewport container
let viewport = document.createElement('div');
viewport.className = 'viewport';
body.appendChild(viewport);

// top menu
let menuDiv = document.createElement('div');
menuDiv.className = 'menu';
viewport.appendChild(menuDiv);

// menu links
let aboutLink = document.createElement('a');
aboutLink.innerHTML = 'About';
aboutLink.href = '/';
aboutLink.className = 'about';
menuDiv.appendChild(aboutLink);

let storeLink = document.createElement('a');
storeLink.innerHTML = 'Store';
storeLink.href = '/';
storeLink.className = 'store';
menuDiv.appendChild(storeLink);

// picture container above search bar
let picDiv = document.createElement('div');
picDiv.className = 'picContainer';
viewport.appendChild(picDiv);

// picture
let picture = document.createElement('div');
picture.className = 'pic';
picture.innerHTML = 'Picture goes here'
picDiv.appendChild(picture);

// search bar container
let searchContainer = document.createElement('div');
searchContainer.className = 'searchContainer';
viewport.appendChild(searchContainer);

// search bar
let searchBar = document.createElement('input');
searchBar.className = 'searchBar';
searchBar.type = 'text';
searchBar.placeholder = 'What are you looking for?';
searchContainer.appendChild(searchBar);

// search button
let searchButton = document.createElement('button');
searchButton.className = 'searchButton';
searchButton.innerHTML = 'Search';
searchButton.type = 'submit';
searchButton.onclick = function() {
    window.open('results.html', '_self')
}
searchContainer.appendChild(searchButton);

//results div
let resultsContainer = document.createElement('div');
resultsContainer.className = 'resultsContainer';
resultsContainer.style.display="none";
viewport.appendChild(resultsContainer);

// bottom div
let bottomDiv = document.createElement('div');
bottomDiv.className = 'bottomDiv';
viewport.appendChild(bottomDiv);

// advertising link
let adLink = document.createElement('a');
adLink.innerHTML = 'Advertising'
adLink.className = 'adLink';
adLink.href = '/';
bottomDiv.appendChild(adLink);

// Business link
let bizLink = document.createElement('a');
bizLink.innerHTML = 'Business';
bizLink.className = 'bizLink';
bizLink.href = '/';
bottomDiv.appendChild(bizLink);

//How Link
let howLink = document.createElement('a');
howLink.innerHTML = 'How Search Works';
howLink.className = 'howLink';
howLink.href = '/';
bottomDiv.appendChild(howLink);
    
if (body.id === 'results') {
    //hide picture, change placeholder to search terms
    document.querySelector(".picContainer").style.display="none";
    document.querySelector(".searchBar").placeholder="Search Input";

    //Results Container
    resultsContainer.style.display="inline";
    
    //Output results
    for (i=0;i<20;i++){
        let result=document.createElement("div");
        result.className='result';
        document.querySelector(".resultsContainer").appendChild(result);
    };
};
    
